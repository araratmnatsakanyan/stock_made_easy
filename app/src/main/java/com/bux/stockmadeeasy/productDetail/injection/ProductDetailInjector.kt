package com.bux.stockmadeeasy.productDetail.injection

import android.app.Application
import com.bux.stockmadeeasy.data.injection.NetworkModule
import com.bux.stockmadeeasy.data.injection.RepositoryModule
import com.bux.stockmadeeasy.data.injection.SocketModule
import com.bux.stockmadeeasy.productDetail.ProductDetailActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        (ProductDetailPresenterModule::class),
        (NetworkModule::class),
        (SocketModule::class),
        (RepositoryModule::class)
    ]
)
@Singleton
interface ProductDetailInjector {
    fun inject(app: ProductDetailActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ProductDetailInjector

        fun presenterModule(presenterModule: ProductDetailPresenterModule): Builder
    }
}
