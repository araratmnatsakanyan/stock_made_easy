package com.bux.stockmadeeasy.productDetail.injection

import com.bux.stockmadeeasy.data.ProductRepository
import com.bux.stockmadeeasy.productDetail.ProductDetailContract
import com.bux.stockmadeeasy.productDetail.ProductDetailPresenter
import dagger.Module
import dagger.Provides

@Module
class ProductDetailPresenterModule(private val view: ProductDetailContract.View) {
    @Provides
    fun provideProductDetailPresenter(productRepository: ProductRepository): ProductDetailPresenter {
        return ProductDetailPresenter(view, productRepository)
    }
}
