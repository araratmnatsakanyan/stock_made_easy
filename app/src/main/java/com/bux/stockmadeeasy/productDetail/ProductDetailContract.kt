package com.bux.stockmadeeasy.productDetail

import com.bux.stockmadeeasy.base.BaseView
import com.bux.stockmadeeasy.data.rest.model.Product

class ProductDetailContract {
    interface Presenter {
        fun loadProduct(product: Product?)
    }

    interface View : BaseView {
        fun setLiveUpdateIndicatorVisibility(visibility: Boolean)

        fun showProductDetails(product: Product)

        fun showProductNotFoundOverlay()

        fun updateProductCurrentPrice(updatedPrice: Double)
    }
}
