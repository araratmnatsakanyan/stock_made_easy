package com.bux.stockmadeeasy.productDetail

import com.bux.stockmadeeasy.base.BasePresenter
import com.bux.stockmadeeasy.data.ProductRepository
import com.bux.stockmadeeasy.data.Result
import com.bux.stockmadeeasy.data.SubscriptionConnectionStatus
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.SocketResult
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ProductDetailPresenter @Inject constructor(
    view: ProductDetailContract.View,
    private val productRepository: ProductRepository
) : BasePresenter<ProductDetailContract.View>(view),
    ProductDetailContract.Presenter {
    private var awaitingSubscribe: Subscribe? = null

    private var productDisposable: Disposable? = null

    private var serviceConnectionDisposable: Disposable? = null

    private var socketStatus: SubscriptionConnectionStatus =
        SubscriptionConnectionStatus.DISCONNECTED

    init {
        observeBuxServiceConnection()
        observeProduct()
    }

    override fun destroy() {
        super.destroy()

        productDisposable?.dispose()
        serviceConnectionDisposable?.dispose()
    }

    override fun loadProduct(product: Product?) {
        product?.let {
            view.showProductDetails(it)
            subscribeToProduct(it.securityId)
        } ?: run {
            view.showProductNotFoundOverlay()
        }
    }

    fun emitSubscribe(subscribe: Subscribe?) {
        subscribe?.let { productRepository.subscribe(it) }
    }

    fun setSocketStatus(socketStatus: SubscriptionConnectionStatus) {
        this.socketStatus = socketStatus
    }

    private fun computeQuoteResult(result: Result<ProductSubscriptionResultBody>) {
        when (result) {
            is Result.Success -> {
                if (result.data.type == SocketResult.TYPE_TRADING) {
                    result.data.currentPrice?.let { view.updateProductCurrentPrice(it) }
                }
            }
            is Result.Error -> {
                when (result.resultError.errorType) {
                    else -> handleError(result)
                }
            }
        }
    }

    private fun observeProduct() {
        productDisposable = productRepository.observeProduct().subscribe {
            computeQuoteResult(it)
        }
    }

    private fun observeBuxServiceConnection() {
        serviceConnectionDisposable = productRepository.observeBuxServiceConnection()?.subscribe {
            when (it) {
                is Result.Success -> {
                    awaitingSubscribe?.let { subscribe ->
                        emitSubscribe(subscribe)
                    }

                    view.setLiveUpdateIndicatorVisibility(true)
                }
                is Result.Error -> {
                    view.setLiveUpdateIndicatorVisibility(false)
                }
            }
        }
    }

    private fun subscribeToProduct(productId: String) {
        val subscribe = Subscribe(
            listOf("trading.product.$productId"),
            listOf()
        )

        if (socketStatus == SubscriptionConnectionStatus.CONNECTED) {
            emitSubscribe(subscribe)
        } else {
            awaitingSubscribe = subscribe
        }
    }
}
