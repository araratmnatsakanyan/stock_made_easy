package com.bux.stockmadeeasy.productDetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.bux.stockmadeeasy.R
import com.bux.stockmadeeasy.base.BaseActivity
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.databinding.ActivityProductDetailBinding
import com.bux.stockmadeeasy.productDetail.injection.DaggerProductDetailInjector
import com.bux.stockmadeeasy.productDetail.injection.ProductDetailPresenterModule
import javax.inject.Inject

class ProductDetailActivity : BaseActivity(),
    ProductDetailContract.View {
    private lateinit var binding: ActivityProductDetailBinding

    @Inject
    lateinit var presenter: ProductDetailPresenter

    companion object {
        private const val EXTRA_PRODUCT = "PRODUCT"

        fun newIntent(context: Context, product: Product): Intent {
            val intent = Intent(context, ProductDetailActivity::class.java)
            intent.putExtra(EXTRA_PRODUCT, product)

            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerProductDetailInjector
            .builder()
            .application(application)
            .presenterModule(ProductDetailPresenterModule(this))
            .build()
            .inject(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail)
        presenter.initialize()

        val product = intent.getSerializableExtra(EXTRA_PRODUCT) as? Product
        presenter.loadProduct(product)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun setLiveUpdateIndicatorVisibility(visibility: Boolean) {
        binding.liveUpdateAvailability = visibility
    }

    override fun showErrorForConnection() {
        // Intentionally left blank as we support socket connection indicator for now
    }

    override fun showProductDetails(product: Product) {
        binding.product = product
    }

    override fun showProductNotFoundOverlay() {
        binding.noProductOverlayVisibility = true
    }

    override fun updateProductCurrentPrice(updatedPrice: Double) {
        binding.updatedPrice = updatedPrice
    }
}
