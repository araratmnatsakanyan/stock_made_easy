package com.bux.stockmadeeasy.productList.listener

import com.bux.stockmadeeasy.data.rest.model.Product

interface OnProductClickListener {
    fun onProductClick(product: Product)
}
