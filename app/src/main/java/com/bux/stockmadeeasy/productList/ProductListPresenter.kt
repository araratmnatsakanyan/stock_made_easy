package com.bux.stockmadeeasy.productList

import com.bux.stockmadeeasy.base.BasePresenter
import com.bux.stockmadeeasy.data.ProductRepository
import com.bux.stockmadeeasy.data.Result
import com.bux.stockmadeeasy.data.rest.model.Product
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ProductListPresenter @Inject constructor(
    view: ProductListContract.View,
    private val productRepository: ProductRepository
) : BasePresenter<ProductListContract.View>(view),
    ProductListContract.Presenter {
    override fun resume() {
        super.resume()

        loadProducts()
    }

    override fun onProductClick(product: Product) {
        view.showProductDetails(product)
    }

    override fun onRefresh() {
        loadProducts()
    }

    fun computeProductsResult(result: Result<List<Product>>?) {
        when (result) {
            is Result.Success -> {
                view.showListOfProducts(result.data)
                view.setEmptyListOverlayVisibility(result.data.isEmpty())
            }
            is Result.Error -> handleError(result)
        }

        view.setLoadingVisibility(false)
        view.setSwipeLayoutRefreshingVisibility(false)
    }

    private fun loadProducts() {
        view.setLoadingVisibility(true)
        CoroutineScope(Dispatchers.IO).launch {
            val productsResult = productRepository.getProducts()
            withContext(Dispatchers.Main) {
                computeProductsResult(productsResult)
            }
        }
    }
}
