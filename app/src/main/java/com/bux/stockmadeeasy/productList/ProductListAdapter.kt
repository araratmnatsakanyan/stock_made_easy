package com.bux.stockmadeeasy.productList

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bux.stockmadeeasy.R
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.databinding.ItemStockBinding
import com.bux.stockmadeeasy.productList.listener.OnProductClickListener
import javax.inject.Inject

class ProductListAdapter @Inject constructor(
    private val context: Context,
    private val onProductClickListener: OnProductClickListener
) : RecyclerView.Adapter<ProductListAdapter.StockViewHolder>() {
    private var products: List<Product> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemStockBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_stock,
            parent,
            false
        )

        return StockViewHolder(binding, onProductClickListener)
    }

    override fun getItemCount() = products.size

    override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
        holder.bind(products[position])
    }

    fun updateStocks(products: List<Product>) {
        this.products = products
    }

    class StockViewHolder(
        private val binding: ItemStockBinding,
        private val onProductClickListener: OnProductClickListener
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: Product) {
            binding.product = product
            binding.onProductClickListener = onProductClickListener
            binding.executePendingBindings()
        }
    }
}
