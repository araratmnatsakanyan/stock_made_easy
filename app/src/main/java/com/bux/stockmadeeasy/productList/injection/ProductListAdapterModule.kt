package com.bux.stockmadeeasy.productList.injection

import android.content.Context
import com.bux.stockmadeeasy.productList.ProductListAdapter
import com.bux.stockmadeeasy.productList.listener.OnProductClickListener
import dagger.Module
import dagger.Provides

@Module
class ProductListAdapterModule(
    private val context: Context,
    private val onProductClickListener: OnProductClickListener
) {
    @Provides
    fun provideStocksAdapter(): ProductListAdapter {
        return ProductListAdapter(context, onProductClickListener)
    }
}
