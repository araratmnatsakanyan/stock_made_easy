package com.bux.stockmadeeasy.productList

import com.bux.stockmadeeasy.base.BaseView
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.productList.listener.OnProductClickListener

class ProductListContract {
    interface Presenter : OnProductClickListener {
        fun onRefresh()
    }

    interface View : BaseView {
        fun setEmptyListOverlayVisibility(visibility: Boolean)

        fun showListOfProducts(products: List<Product>)

        fun setLoadingVisibility(visibility: Boolean)

        fun showProductDetails(product: Product)

        fun setSwipeLayoutRefreshingVisibility(visibility: Boolean)
    }
}
