// Safe here as method are used by Data binding
@file:Suppress("unused")

package com.bux.stockmadeeasy.productList.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bux.stockmadeeasy.productList.ProductListAdapter

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: ProductListAdapter) {
    view.adapter = adapter
}

@BindingAdapter("layoutManager")
fun setAdapter(view: RecyclerView, layoutManager: RecyclerView.LayoutManager) {
    view.layoutManager = layoutManager
}
