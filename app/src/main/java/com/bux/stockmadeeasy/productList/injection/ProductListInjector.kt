package com.bux.stockmadeeasy.productList.injection

import android.app.Application
import com.bux.stockmadeeasy.data.injection.NetworkModule
import com.bux.stockmadeeasy.data.injection.RepositoryModule
import com.bux.stockmadeeasy.data.injection.SocketModule
import com.bux.stockmadeeasy.productList.ProductListActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        (ProductListPresenterModule::class),
        (ProductListAdapterModule::class),
        (NetworkModule::class),
        (SocketModule::class),
        (RepositoryModule::class)
    ]
)
@Singleton
interface ProductListInjector {
    fun inject(app: ProductListActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ProductListInjector

        fun adapterModule(adapterModule: ProductListAdapterModule): Builder

        fun presenterModule(presenterModule: ProductListPresenterModule): Builder
    }
}
