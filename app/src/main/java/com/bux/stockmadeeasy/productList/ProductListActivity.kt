package com.bux.stockmadeeasy.productList

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bux.stockmadeeasy.R
import com.bux.stockmadeeasy.base.BaseActivity
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.databinding.ActivityProductListBindingImpl
import com.bux.stockmadeeasy.productDetail.ProductDetailActivity
import com.bux.stockmadeeasy.productList.injection.DaggerProductListInjector
import com.bux.stockmadeeasy.productList.injection.ProductListAdapterModule
import com.bux.stockmadeeasy.productList.injection.ProductListPresenterModule
import com.bux.stockmadeeasy.productList.listener.OnProductClickListener
import kotlinx.android.synthetic.main.activity_product_list.*
import javax.inject.Inject


class ProductListActivity : BaseActivity(), ProductListContract.View, OnProductClickListener {
    private lateinit var binding: ActivityProductListBindingImpl

    @Inject
    lateinit var presenter: ProductListPresenter

    @Inject
    lateinit var stocksAdapter: ProductListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerProductListInjector
            .builder()
            .application(application)
            .presenterModule(ProductListPresenterModule(this))
            .adapterModule(ProductListAdapterModule(this, this))
            .build()
            .inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_list)
        binding.adapter = stocksAdapter
        binding.layoutManager = LinearLayoutManager(this)
        stocksRecyclerView.addItemDecoration(
            DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL
            )
        )
        presenter.initialize()
        initListeners()
    }

    private fun initListeners() {
        swipeRefreshLayout.setOnRefreshListener { presenter.onRefresh() }
    }

    override fun onProductClick(product: Product) {
        presenter.onProductClick(product)
    }

    override fun showErrorForConnection() {
        setEmptyListOverlayVisibility(true)
    }

    override fun showErrorForServer(errorMessage: String) {
        super.showErrorForServer(errorMessage)

        setEmptyListOverlayVisibility(true)
    }

    override fun showListOfProducts(products: List<Product>) {
        stocksAdapter.updateStocks(products)
    }

    override fun showProductDetails(product: Product) {
        startActivity(ProductDetailActivity.newIntent(this, product))
    }

    override fun setEmptyListOverlayVisibility(visibility: Boolean) {
        binding.emptyOverlayVisibility = visibility
    }

    override fun setLoadingVisibility(visibility: Boolean) {
        if (!swipeRefreshLayout.isRefreshing) {
            binding.loadingProgressVisibility = visibility
        }
    }

    override fun setSwipeLayoutRefreshingVisibility(visibility: Boolean) {
        swipeRefreshLayout.isRefreshing = visibility
    }
}
