package com.bux.stockmadeeasy.productList.injection

import com.bux.stockmadeeasy.data.ProductRepository
import com.bux.stockmadeeasy.productList.ProductListContract
import com.bux.stockmadeeasy.productList.ProductListPresenter
import dagger.Module
import dagger.Provides

@Module
class ProductListPresenterModule(private val view: ProductListContract.View) {
    @Provides
    fun provideStocksPresenter(productRepository: ProductRepository): ProductListPresenter {
        return ProductListPresenter(view, productRepository)
    }
}
