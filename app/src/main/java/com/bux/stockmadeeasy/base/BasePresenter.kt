package com.bux.stockmadeeasy.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.bux.stockmadeeasy.data.Result
import com.bux.stockmadeeasy.data.rest.model.ResultError

/**
 * Base presenter any presenter of the application must extend. It provides initial injections and
 * required methods.
 */
abstract class BasePresenter<out V : BaseView>(
    protected val view: V
) : LifecycleObserver, LifecycleOwner {

    override fun getLifecycle() = view.getLifecycle()

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun destroy() {
        view.getLifecycle().removeObserver(this)
    }

    fun initialize() {
        view.getLifecycle().addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun pause() {
        // To be overridden
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun resume() {
        // To be overridden
    }

    fun handleError(error: Result.Error?) {
        val errorMessage = error?.resultError?.buxResultError?.message

        when (error?.resultError?.errorType) {
            ResultError.ErrorType.CONNECTION -> view.showErrorForConnection()
            ResultError.ErrorType.SERVER -> errorMessage?.let { view.showErrorForServer(it) }
        }
    }
}
