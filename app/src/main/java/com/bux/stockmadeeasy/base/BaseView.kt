package com.bux.stockmadeeasy.base

import androidx.lifecycle.Lifecycle

/**
 * Base view any view must implement.
 */
interface BaseView {
    fun getLifecycle(): Lifecycle

    fun showErrorForConnection()

    fun showErrorMessage(errorMessage: String)

    fun showErrorForServer(errorMessage: String)
}
