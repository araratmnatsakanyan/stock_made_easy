package com.bux.stockmadeeasy.data.injection

import com.bux.stockmadeeasy.data.*
import com.bux.stockmadeeasy.data.rest.source.ProductsApi
import com.bux.stockmadeeasy.data.socket.BuxSubscriptionService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module which provides all required dependencies about rest api
 */
@Module
object RepositoryModule {
    @Provides
    @JvmStatic
    @Singleton
    fun provideProductRepository(
        productApiDataSource: ProductApiDataSource,
        productSubscriptionDataSource: ProductSubscriptionDataSource
    ): ProductRepository {
        return BuxProductRepository(productApiDataSource, productSubscriptionDataSource)
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideProductSubscriptionDataSource(
        buxSubscriptionService: BuxSubscriptionService
    ): ProductSubscriptionDataSource {
        return BuxProductSubscriptionDataSource(buxSubscriptionService)
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideProductApiDataSource(
        productsApi: ProductsApi
    ): ProductApiDataSource {
        return BuxProductApiDataSource(productsApi)
    }
}
