package com.bux.stockmadeeasy.data.socket.model

import com.google.gson.annotations.SerializedName

data class ProductSubscriptionResultBody(
    @SerializedName("currentPrice") val currentPrice: Double?,
    @SerializedName("securityId") val securityId: String?,
    var type: String? = null
)
