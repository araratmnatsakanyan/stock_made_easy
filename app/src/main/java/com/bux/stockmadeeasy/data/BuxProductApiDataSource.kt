package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.data.rest.model.BuxResultError
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.rest.model.ResultError
import com.bux.stockmadeeasy.data.rest.source.ProductsApi
import com.google.gson.Gson

class BuxProductApiDataSource(private val productsApi: ProductsApi) : ProductApiDataSource {
    override suspend fun getProducts(): Result<List<Product>>? {
        try {
            val response = productsApi.getProducts()
            val responseBody = response.body()

            return if (response.isSuccessful && responseBody != null) {
                Result.Success(responseBody)
            } else {
                val resultError = when (response.code()) {
                    500 -> ResultError(
                        ResultError.ErrorType.SERVER,
                        BuxResultError(errorCode = "500")
                    )
                    else -> {
                        /*
                        To not create an instance of the Gson class we can create an interface
                        which will be dedicated for json operations usage. To then change it easily
                        if needed to another library
                        */
                        val buxResultError = Gson().fromJson(
                            response.errorBody().toString(),
                            BuxResultError::class.java
                        )

                        ResultError(ResultError.ErrorType.SERVER, buxResultError)
                    }
                }

                return Result.Error(resultError)
            }
        } catch (exception: Exception) {
            val resultError = ResultError(
                ResultError.ErrorType.CONNECTION,
                BuxResultError()
            )

            return Result.Error(resultError)
        }
    }
}
