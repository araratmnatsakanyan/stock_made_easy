package com.bux.stockmadeeasy.data.rest.source

import com.bux.stockmadeeasy.data.rest.model.Product
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * The interface which provides methods to get result
 */
interface ProductsApi {
    @GET("/core/23/products/{productId}")
    suspend fun getProduct(
        @Path("productId") productId: String
    ): Product

    @GET("/core/23/products")
    suspend fun getProducts(): Response<List<Product>>
}
