package com.bux.stockmadeeasy.data.rest.util

import com.bux.stockmadeeasy.data.rest.model.Product

fun Product.getPriceDifferenceInPercents(): Double {
    return getPriceDifferenceInPercents(currentPrice.amount)
}

fun Product.getPriceDifferenceInPercentsToShow(): String {
    return getPriceDifferenceInPercentsToShow(currentPrice.amount)
}

fun Product.getPriceDifferenceInPercents(priceToCompare: Double): Double {
    val differenceOfAmounts = priceToCompare - closingPrice.amount

    return if (closingPrice.amount.toInt() == 0) {
        100.0
    } else {
        differenceOfAmounts / closingPrice.amount * 100
    }
}

fun Product.getPriceDifferenceInPercentsToShow(priceToCompare: Double): String {
    val percentage = getPriceDifferenceInPercents(priceToCompare)

    return "%.2f".format(percentage) + "%"
}
