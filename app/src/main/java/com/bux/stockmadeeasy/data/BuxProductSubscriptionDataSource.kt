package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.data.rest.model.BuxResultError
import com.bux.stockmadeeasy.data.rest.model.ResultError
import com.bux.stockmadeeasy.data.socket.BuxSubscriptionService
import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.SocketResult
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.tinder.scarlet.WebSocket
import io.reactivex.Flowable

class BuxProductSubscriptionDataSource(
    private val buxSubscriptionService: BuxSubscriptionService
) : ProductSubscriptionDataSource {
    override fun observeProduct(): Flowable<Result<ProductSubscriptionResultBody>> {
        return buxSubscriptionService.observeProduct().map {
            if (it.type == SocketResult.TYPE_TRADING || it.type == SocketResult.TYPE_SUCCESS) {
                generateSuccessResult(it)
            } else {
                generateErrorResult(it.body)
            }
        }
    }

    override fun observeWebSocketEvent(): Flowable<Result<String>>? {
        return buxSubscriptionService.observeWebSocketEvent().map {
            when (it) {
                is WebSocket.Event.OnConnectionOpened<*> -> Result.Success("")
                is WebSocket.Event.OnConnectionFailed -> generateErrorResult(
                    ResultError.ErrorType.CONNECTION
                )
                is WebSocket.Event.OnConnectionClosed -> generateErrorResult(
                    ResultError.ErrorType.CONNECTION
                )
                else -> Result.Success("")
            }
        }
    }

    override fun subscribe(subscribe: Subscribe) {
        buxSubscriptionService.sendSubscribe(subscribe)
    }

    private fun generateErrorResult(errorType: ResultError.ErrorType): Result.Error {
        val buxResultError = BuxResultError()
        val resultError = ResultError(errorType = errorType, buxResultError = buxResultError)

        return Result.Error(resultError)
    }

    private fun generateErrorResult(jsonObject: JsonObject): Result.Error {
        val buxResultError = Gson().fromJson(jsonObject, BuxResultError::class.java)
        val resultError = ResultError(buxResultError = buxResultError)

        return Result.Error(resultError)
    }

    private fun generateSuccessResult(
        socketResult: SocketResult
    ): Result.Success<ProductSubscriptionResultBody> {
        val buxResultSuccess = Gson().fromJson(
            socketResult.body,
            ProductSubscriptionResultBody::class.java
        )
        buxResultSuccess.type = socketResult.type

        return Result.Success(buxResultSuccess)
    }
}
