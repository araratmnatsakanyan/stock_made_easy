package com.bux.stockmadeeasy.data.injection

import android.app.Application
import com.bux.stockmadeeasy.BuildConfig
import com.bux.stockmadeeasy.data.socket.BuxSubscriptionService
import com.tinder.scarlet.Lifecycle
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.lifecycle.android.AndroidLifecycle
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import javax.inject.Singleton


/**
 * Module which provides all required dependencies about socket connection
 */
@Module
object SocketModule {
    @Provides
    @Reusable
    fun provideLifecycle(application: Application): Lifecycle {
        return AndroidLifecycle.ofApplicationForeground(application)
    }

    @Provides
    @Singleton
    internal fun provideScarletService(scarlet: Scarlet): BuxSubscriptionService {
        return scarlet.create(BuxSubscriptionService::class.java)
    }

    @Provides
    @Singleton
    internal fun provideScarletInterface(client: OkHttpClient, lifecycle: Lifecycle): Scarlet {
        return Scarlet.Builder()
            .webSocketFactory(client.newWebSocketFactory(BuildConfig.SOCKET_HOST))
            .addMessageAdapterFactory(GsonMessageAdapter.Factory())
            .addStreamAdapterFactory(RxJava2StreamAdapterFactory())
            .lifecycle(lifecycle)
            .build()
    }
}
