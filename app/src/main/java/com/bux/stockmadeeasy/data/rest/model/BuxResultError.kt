package com.bux.stockmadeeasy.data.rest.model

import com.google.gson.annotations.SerializedName

data class BuxResultError(
    @SerializedName("developerMessage") val developerMessage: String? = null,
    @SerializedName("errorCode") val errorCode: String? = null,
    @SerializedName("message") val message: String? = null
)
