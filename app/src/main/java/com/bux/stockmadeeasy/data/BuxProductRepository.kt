package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import io.reactivex.Flowable

typealias SubscriptionConnectionStatus = ProductRepository.SubscriptionConnectionStatus

class BuxProductRepository(
    private val productApiDataSource: ProductApiDataSource,
    private val productSubscriptionDataSource: ProductSubscriptionDataSource
) : ProductRepository {
    override suspend fun getProducts(): Result<List<Product>>? = productApiDataSource.getProducts()

    override fun observeBuxServiceConnection(): Flowable<Result<String>>? {
        return productSubscriptionDataSource.observeWebSocketEvent()
    }

    override fun observeProduct(): Flowable<Result<ProductSubscriptionResultBody>> {
        return productSubscriptionDataSource.observeProduct()
    }

    override fun subscribe(subscribe: Subscribe) {
        productSubscriptionDataSource.subscribe(subscribe)
    }
}
