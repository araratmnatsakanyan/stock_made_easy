package com.bux.stockmadeeasy.data.rest.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Product(
    @SerializedName("symbol") @Expose val symbol: String,
    @SerializedName("securityId") @Expose val securityId: String,
    @SerializedName("displayName") @Expose val displayName: String,
    @SerializedName("currentPrice") @Expose val currentPrice: Price,
    @SerializedName("closingPrice") @Expose val closingPrice: Price
) : Serializable
