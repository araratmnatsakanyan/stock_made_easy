package com.bux.stockmadeeasy.data.rest.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

data class Price(
    @SerializedName("currency") @Expose val currency: String,
    @SerializedName("decimals") @Expose val decimals: Int,
    @SerializedName("amount") @Expose val amount: Double
) : Serializable
