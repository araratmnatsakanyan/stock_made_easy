package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import io.reactivex.Flowable

interface ProductSubscriptionDataSource {
    fun observeProduct(): Flowable<Result<ProductSubscriptionResultBody>>

    fun observeWebSocketEvent(): Flowable<Result<String>>?

    fun subscribe(subscribe: Subscribe)
}
