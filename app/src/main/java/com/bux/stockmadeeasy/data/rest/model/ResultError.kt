package com.bux.stockmadeeasy.data.rest.model

data class ResultError(
    val errorType: ErrorType = ErrorType.UNKNOWN,
    val buxResultError: BuxResultError
) {
    enum class ErrorType {
        SERVER,
        CONNECTION,
        UNKNOWN
    }
}
