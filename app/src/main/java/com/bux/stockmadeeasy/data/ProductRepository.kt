package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import io.reactivex.Flowable

interface ProductRepository {
    suspend fun getProducts(): Result<List<Product>>?

    fun observeBuxServiceConnection(): Flowable<Result<String>>?

    fun observeProduct(): Flowable<Result<ProductSubscriptionResultBody>>

    fun subscribe(subscribe: Subscribe)

    enum class SubscriptionConnectionStatus {
        CONNECTED,
        DISCONNECTED
    }
}
