package com.bux.stockmadeeasy.data.injection

import com.bux.stockmadeeasy.BuildConfig
import com.bux.stockmadeeasy.data.rest.source.ProductsApi
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Module which provides all required dependencies about rest api
 */
@Module
object NetworkModule {
    @Provides
    @Singleton
    internal fun provideProductsApi(retrofit: Retrofit): ProductsApi {
        return retrofit.create(ProductsApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRetrofitInterface(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_HOST)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    @Reusable
    fun provideOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()

        if (BuildConfig.LOGGING) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }

        httpClient.addInterceptor {
            val original: Request = it.request()

            val request: Request = original.newBuilder()
                .header("Authorization", BuildConfig.AUTHORIZATION)
                .header("Accept", "application/json")
                .header("Accept-Language", "nl-NL,en;q=0.8")
                .build()

            it.proceed(request)
        }

        return httpClient.build()
    }
}
