package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.data.rest.model.Product

interface ProductApiDataSource {
    suspend fun getProducts(): Result<List<Product>>?
}
