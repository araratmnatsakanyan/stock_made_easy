package com.bux.stockmadeeasy.data.socket.model

data class Subscribe(
    val subscribeTo: List<String>,
    val unsubscribeFrom: List<String>
)
