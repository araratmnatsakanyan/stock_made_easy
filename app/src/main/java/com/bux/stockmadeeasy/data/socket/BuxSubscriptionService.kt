package com.bux.stockmadeeasy.data.socket

import com.bux.stockmadeeasy.data.socket.model.SocketResult
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Flowable

interface BuxSubscriptionService {
    @Receive
    fun observeProduct(): Flowable<SocketResult>

    @Receive
    fun observeWebSocketEvent(): Flowable<WebSocket.Event>

    @Send
    fun sendSubscribe(subscribe: Subscribe)
}
