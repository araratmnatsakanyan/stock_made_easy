package com.bux.stockmadeeasy.data.rest.util

import com.bux.stockmadeeasy.data.rest.model.Price
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

fun Price.getPriceToShow(): String {
    return getFormattedCurrencyString(currency, amount, decimals)
}

fun getFormattedCurrencyString(
    isoCurrencyCode: String?,
    amount: Double,
    decimals: Int
): String {
    val format = localeFormat(Currency.getInstance(isoCurrencyCode), decimals)
    format.format(amount)

    return format.format(amount)
}

fun localeFormat(currency: Currency, decimals: Int): NumberFormat {
    val format: NumberFormat = NumberFormat.getCurrencyInstance()

    if (format is DecimalFormat) {
        val decimalFormatSymbols: DecimalFormatSymbols = DecimalFormat().decimalFormatSymbols
        format.applyPattern(generateNumberSigns(decimals))
        decimalFormatSymbols.currencySymbol = currency.symbol
        format.decimalFormatSymbols = decimalFormatSymbols
    }

    return format
}

fun generateNumberSigns(count: Int): String? {
    var sign = "¤#0."

    for (i in 0 until count) {
        sign += "0"
    }

    sign += "#"

    return sign
}
