package com.bux.stockmadeeasy.data.socket.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class SocketResult(
    @SerializedName("t")
    val type: String,
    val body: JsonObject
) {
    companion object {
        const val TYPE_FAILED = "connect.failed"
        const val TYPE_SUCCESS = "connect.connected"
        const val TYPE_TRADING = "trading.quote"
    }
}
