package com.bux.stockmadeeasy.productList

import com.bux.stockmadeeasy.data.BuxProductRepository
import com.bux.stockmadeeasy.data.ProductRepository
import com.bux.stockmadeeasy.data.Result
import com.bux.stockmadeeasy.data.dataSource.FakeProductApiDataSource
import com.bux.stockmadeeasy.data.dataSource.FakeProductSubscriptionDataSource
import com.bux.stockmadeeasy.data.rest.model.BuxResultError
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.rest.model.ResultError
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ProductListPresenterTest {
    private lateinit var productListPresenter: ProductListPresenter

    private lateinit var productRepository: ProductRepository

    private lateinit var view: ProductListContract.View

    @Before
    fun createPresenter() {
        MockitoAnnotations.initMocks(this)
        view = FakeProductListActivity()
        productRepository = BuxProductRepository(
            FakeProductApiDataSource(),
            FakeProductSubscriptionDataSource()
        )
        productListPresenter = ProductListPresenter(view, productRepository)
    }

    @Test
    fun computeProductsResult_error_invokeHandleError() {
        val productListPresenterSpy: ProductListPresenter = spy(productListPresenter)
        val error = generateError()

        doCallRealMethod().`when`(productListPresenterSpy).computeProductsResult(error)
        productListPresenterSpy.computeProductsResult(error)

        verify(productListPresenterSpy).handleError(error)
    }

    @Test
    fun computeProductsResult_success_noInvokeHandleError() {
        val productListPresenterSpy: ProductListPresenter = spy(productListPresenter)
        val success = generateSuccess()

        doCallRealMethod().`when`(productListPresenterSpy).computeProductsResult(success)
        productListPresenterSpy.computeProductsResult(success)

        verify(productListPresenterSpy, never()).handleError(any())
    }

    private fun generateSuccess(): Result.Success<List<Product>> {
        return Result.Success(listOf())
    }

    private fun generateError(): Result.Error {
        return Result.Error(ResultError(buxResultError = BuxResultError()))
    }
}
