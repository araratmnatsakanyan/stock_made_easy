package com.bux.stockmadeeasy.productList

import androidx.appcompat.app.AppCompatActivity
import com.bux.stockmadeeasy.data.rest.model.Product

class FakeProductListActivity : AppCompatActivity(), ProductListContract.View {
    override fun setEmptyListOverlayVisibility(visibility: Boolean) {
        // Fake implementation
    }

    override fun showListOfProducts(products: List<Product>) {
        // Fake implementation
    }

    override fun setLoadingVisibility(visibility: Boolean) {
        // Fake implementation
    }

    override fun showProductDetails(product: Product) {
        // Fake implementation
    }

    override fun setSwipeLayoutRefreshingVisibility(visibility: Boolean) {
        // Fake implementation
    }

    override fun showErrorForConnection() {
        // Fake implementation
    }

    override fun showErrorMessage(errorMessage: String) {
        // Fake implementation
    }

    override fun showErrorForServer(errorMessage: String) {
        // Fake implementation
    }
}
