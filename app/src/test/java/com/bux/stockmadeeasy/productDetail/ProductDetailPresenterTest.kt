package com.bux.stockmadeeasy.productDetail

import com.bux.stockmadeeasy.data.BuxProductRepository
import com.bux.stockmadeeasy.data.ProductRepository
import com.bux.stockmadeeasy.data.dataSource.FakeProductApiDataSource
import com.bux.stockmadeeasy.data.dataSource.FakeProductSubscriptionDataSource
import com.bux.stockmadeeasy.data.rest.model.Price
import com.bux.stockmadeeasy.data.rest.model.Product
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ProductDetailPresenterTest {
    private lateinit var productDetailPresenter: ProductDetailPresenter

    private lateinit var productRepository: ProductRepository

    private lateinit var view: ProductDetailContract.View

    @Before
    fun createPresenter() {
        MockitoAnnotations.initMocks(this)
        view = FakeProductDetailActivity()
        productRepository = BuxProductRepository(
            FakeProductApiDataSource(),
            FakeProductSubscriptionDataSource()
        )
        productDetailPresenter = ProductDetailPresenter(view, productRepository)
    }

    @Test
    fun observeSocketEvent_successfulEvent_callSubscribe() {
        val productDetailPresenterSpy: ProductDetailPresenter = spy(productDetailPresenter)
        val product = generateProduct("Test", "Test")

        productDetailPresenterSpy.setSocketStatus(ProductRepository.SubscriptionConnectionStatus.DISCONNECTED)
        doCallRealMethod().`when`(productDetailPresenterSpy).loadProduct(product)
        productDetailPresenterSpy.loadProduct(product)

        verify(productDetailPresenterSpy, never()).emitSubscribe(any())
    }

    @Test
    fun observeSocketEvent_successfulEvent_nullProduct_callSubscribe() {
        val productDetailPresenterSpy: ProductDetailPresenter = spy(productDetailPresenter)

        productDetailPresenterSpy.setSocketStatus(ProductRepository.SubscriptionConnectionStatus.CONNECTED)
        doCallRealMethod().`when`(productDetailPresenterSpy).loadProduct(null)
        productDetailPresenterSpy.loadProduct(null)

        verify(productDetailPresenterSpy, never()).emitSubscribe(any())
    }

    @Test
    fun observeSocketEvent_unsuccessfulEvent_notCallingSubscribe() {
        val productDetailPresenterSpy: ProductDetailPresenter = spy(productDetailPresenter)
        val product = generateProduct("Test", "Test")

        productDetailPresenterSpy.setSocketStatus(ProductRepository.SubscriptionConnectionStatus.CONNECTED)
        doCallRealMethod().`when`(productDetailPresenterSpy).loadProduct(product)
        productDetailPresenterSpy.loadProduct(product)

        verify(productDetailPresenterSpy).emitSubscribe(any())
    }

    private fun generateProduct(securityId: String, displayName: String): Product {
        val price = Price("EUR", 2, 0.0)

        return Product("EUR", securityId, displayName, price, price)
    }
}
