package com.bux.stockmadeeasy.productDetail

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.bux.stockmadeeasy.data.rest.model.Product

class FakeProductDetailActivity : AppCompatActivity(), ProductDetailContract.View {
    override fun setLiveUpdateIndicatorVisibility(visibility: Boolean) {
        // Fake implementation
    }

    override fun showProductDetails(product: Product) {
        // Fake implementation
    }

    override fun showProductNotFoundOverlay() {
        // Fake implementation
    }

    override fun updateProductCurrentPrice(updatedPrice: Double) {
        // Fake implementation
    }

    override fun showErrorForConnection() {
        // Fake implementation
    }

    override fun showErrorMessage(errorMessage: String) {
        // Fake implementation
    }

    override fun showErrorForServer(errorMessage: String) {
        // Fake implementation
    }
}
