package com.bux.stockmadeeasy.data

import com.bux.stockmadeeasy.MainCoroutineRule
import com.bux.stockmadeeasy.data.dataSource.FakeProductApiDataSource
import com.bux.stockmadeeasy.data.dataSource.FakeProductSubscriptionDataSource
import com.bux.stockmadeeasy.data.rest.model.Price
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import com.google.common.truth.Truth.assertThat
import io.reactivex.subscribers.TestSubscriber
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class BuxProductRepositoryTest {
    private lateinit var productApiDataSource: FakeProductApiDataSource
    private lateinit var productSubscriptionDataSource: FakeProductSubscriptionDataSource
    private val productList = listOf(
        generateProduct("US500", "sb26496"),
        generateProduct("EUR/USD", "sb26502"),
        generateProduct("Gold", "sb26500"),
        generateProduct("Apple", "sb26513"),
        generateProduct("Deutsche Bank", "sb28248")
    )

    private lateinit var productsRepository: BuxProductRepository

    // Setting the main coroutines dispatcher for unit testing.
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun createRepository() {
        productApiDataSource = FakeProductApiDataSource(productList)
        productSubscriptionDataSource = FakeProductSubscriptionDataSource(productList)
        productsRepository = BuxProductRepository(
            productApiDataSource,
            productSubscriptionDataSource
        )
    }

    @Test
    fun getProducts_emptyRepository() = mainCoroutineRule.runBlockingTest {
        val emptyApiDataSource = FakeProductApiDataSource()
        val emptySubscriptionDataSource = FakeProductSubscriptionDataSource()
        val tasksRepository = BuxProductRepository(emptyApiDataSource, emptySubscriptionDataSource)

        assertThat(tasksRepository.getProducts() is Result.Success).isTrue()
    }

    @Test
    fun getProducts_requestsAllProductsFromRemoteDataSource() = mainCoroutineRule.runBlockingTest {
        val products = productsRepository.getProducts() as Result.Success

        assertThat(products.data).isEqualTo(productList)
    }

    @Test
    fun getProducts_WithDataSourcesUnavailable_returnsError() = mainCoroutineRule.runBlockingTest {
        productApiDataSource.productList = null

        assertThat(productsRepository.getProducts()).isInstanceOf(Result.Error::class.java)
    }

    @Test
    fun subscribeToProduct_productNotExisting_returnsError() = mainCoroutineRule.runBlockingTest {
        productSubscriptionDataSource.productList = null
        productSubscriptionDataSource.subscribe(
            Subscribe(
                listOf("trading.product.notExisting"),
                listOf()
            )
        )

        val testObserver: TestSubscriber<Result<ProductSubscriptionResultBody>>? =
            productSubscriptionDataSource.observeProduct().test()

        testObserver?.awaitTerminalEvent()

        assertThat(testObserver?.values()?.firstOrNull()).isInstanceOf(Result.Error::class.java)
    }

    @Test
    fun subscribeToProduct_productExisting_returnsNotNull() = mainCoroutineRule.runBlockingTest {
        productSubscriptionDataSource.subscribe(
            Subscribe(
                listOf("trading.product" + productList.firstOrNull()?.securityId),
                listOf()
            )
        )

        val testObserver: TestSubscriber<Result<ProductSubscriptionResultBody>>? =
            productSubscriptionDataSource.observeProduct().test()

        testObserver?.awaitTerminalEvent()

        assertThat(testObserver?.values()?.firstOrNull()).isNotNull()
    }

    @Test
    fun subscribeToWebSocketEvent_ConnectionFailed_returnsError() =
        mainCoroutineRule.runBlockingTest {
            productSubscriptionDataSource.setReturnError(true)

            val testObserver: TestSubscriber<Result<String>>? =
                productSubscriptionDataSource.observeWebSocketEvent()?.test()

            testObserver?.awaitTerminalEvent()

            assertThat(testObserver?.values()?.firstOrNull()).isInstanceOf(Result.Error::class.java)
        }

    @Test
    fun subscribeToWebSocketEvent_ConnectionSucceeded_returnsSuccess() =
        mainCoroutineRule.runBlockingTest {
            val testObserver: TestSubscriber<Result<String>>? =
                productSubscriptionDataSource.observeWebSocketEvent()?.test()

            testObserver?.awaitTerminalEvent()

            assertThat(testObserver?.values()?.firstOrNull())
                .isInstanceOf(Result.Success::class.java)
        }

    private fun generateProduct(securityId: String, displayName: String): Product {
        val price = Price("EUR", 2, 0.0)

        return Product("EUR", securityId, displayName, price, price)
    }
}
