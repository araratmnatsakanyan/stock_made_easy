package com.bux.stockmadeeasy.data.util

import com.bux.stockmadeeasy.data.rest.model.Price
import com.bux.stockmadeeasy.data.rest.util.getPriceToShow
import com.google.common.truth.Truth
import org.junit.Test

class PriceUtilTest {
    @Test
    fun getPricePrice_euroZero() {
        val price = generatePrice("EUR", 2, 0.0)

        Truth.assertThat(price.getPriceToShow()).isEqualTo("€0.00")
    }

    @Test
    fun getPricePrice_euroOne() {
        val price = generatePrice("EUR", 2, 1.0)

        Truth.assertThat(price.getPriceToShow()).isEqualTo("€1.00")
    }

    @Test
    fun getPricePrice_euro_threeDecimal() {
        val price = generatePrice("EUR", 3, 52.0)

        Truth.assertThat(price.getPriceToShow()).isEqualTo("€52.000")
    }

    @Test
    fun getPricePrice_euro_zeroDecimal() {
        val price = generatePrice("EUR", 0, 52.0)

        Truth.assertThat(price.getPriceToShow()).isEqualTo("€52")
    }

    @Test
    fun getPricePrice_pound() {
        val price = generatePrice("GBP", 2, 5875.0)

        Truth.assertThat(price.getPriceToShow()).isEqualTo("£5875.00")
    }

    private fun generatePrice(
        currency: String,
        decimals: Int,
        amount: Double
    ) = Price(currency, decimals, amount)
}
