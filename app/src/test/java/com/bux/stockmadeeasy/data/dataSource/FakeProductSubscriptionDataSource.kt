package com.bux.stockmadeeasy.data.dataSource

import com.bux.stockmadeeasy.data.ProductSubscriptionDataSource
import com.bux.stockmadeeasy.data.Result
import com.bux.stockmadeeasy.data.rest.model.BuxResultError
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.rest.model.ResultError
import com.bux.stockmadeeasy.data.socket.model.ProductSubscriptionResultBody
import com.bux.stockmadeeasy.data.socket.model.Subscribe
import io.reactivex.Flowable

class FakeProductSubscriptionDataSource(
    var productList: List<Product>? = null
) : ProductSubscriptionDataSource {
    private var shouldReturnError = false

    private var subscribedProduct: List<Product>? = null

    override fun observeProduct(): Flowable<Result<ProductSubscriptionResultBody>> {
        return if (subscribedProduct.isNullOrEmpty()) {
            Flowable.just(generateErrorResult())
        } else {
            Flowable.just(generateResultBodySuccessResult())
        }
    }

    override fun observeWebSocketEvent(): Flowable<Result<String>>? {
        val event = if (shouldReturnError) {
            generateErrorResult(ResultError.ErrorType.CONNECTION)
        } else {
            generateStringSuccessResult()
        }

        return Flowable.just(event)
    }

    override fun subscribe(subscribe: Subscribe) {
        subscribedProduct = productList?.filter { product ->
            subscribe.subscribeTo.map {
                it.contains(product.securityId)
            }.isNotEmpty()
        }
    }

    private fun generateErrorResult(): Result.Error {
        return generateErrorResult(ResultError.ErrorType.UNKNOWN)
    }

    private fun generateErrorResult(errorType: ResultError.ErrorType): Result.Error {
        val buxResultError = BuxResultError()
        val resultError = ResultError(errorType = errorType, buxResultError = buxResultError)

        return Result.Error(resultError)
    }

    private fun generateResultBodySuccessResult(): Result.Success<ProductSubscriptionResultBody> {
        val product = subscribedProduct?.first()

        val productSubscriptionResultBody = ProductSubscriptionResultBody(
            product?.currentPrice?.amount,
            product?.securityId
        )

        return Result.Success(productSubscriptionResultBody)
    }

    private fun generateStringSuccessResult(): Result.Success<String> {
        return Result.Success("")
    }

    fun setReturnError(value: Boolean) {
        shouldReturnError = value
    }
}
