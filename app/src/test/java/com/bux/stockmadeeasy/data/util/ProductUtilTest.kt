package com.bux.stockmadeeasy.data.util

import com.bux.stockmadeeasy.data.rest.model.Price
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.rest.util.getPriceDifferenceInPercents
import com.bux.stockmadeeasy.data.rest.util.getPriceDifferenceInPercentsToShow
import com.google.common.truth.Truth
import org.junit.Test

class ProductUtilTest {
    @Test
    fun getProductPriceDifference_zeroClosingPrice() {
        val product = generateProduct(0.0, 0.0)

        Truth.assertThat(product.getPriceDifferenceInPercents()).isEqualTo(100.0)
    }

    @Test
    fun getProductPriceDifference_zero() {
        val product = generateProduct(1.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercents()).isEqualTo(0.0)
    }

    @Test
    fun getProductPriceDifference_notZero() {
        val product = generateProduct(1.0, 0.0)

        Truth.assertThat(product.getPriceDifferenceInPercents()).isNotEqualTo(0.0)
    }

    @Test
    fun getProductPriceDifferenceInPercentsToShow_zero() {
        val product = generateProduct(1.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercentsToShow()).isEqualTo("0.00%")
    }

    @Test
    fun getProductPriceDifferenceInPercentsToShow_notZero() {
        val product = generateProduct(2.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercentsToShow()).isNotEqualTo("0.00%")
    }

    @Test
    fun getProductPriceDifferenceInPercentsToShow_hundred() {
        val product = generateProduct(2.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercentsToShow()).isEqualTo("100.00%")
    }

    @Test
    fun getProductPriceDifferenceInPercentsToShowWithArgument_zero() {
        val product = generateProduct(0.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercentsToShow(1.0)).isEqualTo("0.00%")
    }

    @Test
    fun getProductPriceDifferenceInPercentsToShowWithArgument_notZero() {
        val product = generateProduct(0.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercentsToShow(1.5)).isNotEqualTo("0.00%")
    }

    @Test
    fun getProductPriceDifferenceInPercentsToShowWithArgument_hundred() {
        val product = generateProduct(0.0, 1.0)

        Truth.assertThat(product.getPriceDifferenceInPercentsToShow(2.0)).isEqualTo("100.00%")
    }

    private fun generateProduct(currentPrice: Double, closingPrice: Double): Product {
        val current = Price("EUR", 2, currentPrice)
        val closing = Price("EUR", 2, closingPrice)

        return Product("EUR", "Test", "Test", current, closing)
    }
}
