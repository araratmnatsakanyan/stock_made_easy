package com.bux.stockmadeeasy.data.dataSource

import com.bux.stockmadeeasy.data.ProductApiDataSource
import com.bux.stockmadeeasy.data.Result
import com.bux.stockmadeeasy.data.rest.model.BuxResultError
import com.bux.stockmadeeasy.data.rest.model.Product
import com.bux.stockmadeeasy.data.rest.model.ResultError

class FakeProductApiDataSource(
    var productList: List<Product>? = mutableListOf()
) : ProductApiDataSource {
    override suspend fun getProducts(): Result<List<Product>>? {
        return productList?.let {
            Result.Success(it)
        } ?: run {
            Result.Error(ResultError(buxResultError = BuxResultError()))
        }
    }
}
